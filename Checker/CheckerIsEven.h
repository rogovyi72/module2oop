#ifndef MODULE_SEBEK_CHECKERISEVEN_H
#define MODULE_SEBEK_CHECKERISEVEN_H

#include "Checker.h"

class CheckerIsEven : public Checker{
public:
    bool check(std::int32_t number) const noexcept final;
};


#endif //MODULE_SEBEK_CHECKERISEVEN_H
