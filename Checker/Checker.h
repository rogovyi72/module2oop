#ifndef MODULE_SEBEK_CHECKER_H
#define MODULE_SEBEK_CHECKER_H

#include <cstdint>

class Checker{
public:
    virtual bool check(std::int32_t number) const noexcept = 0;
};

#endif //MODULE_SEBEK_CHECKER_H
