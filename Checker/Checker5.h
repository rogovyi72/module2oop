#ifndef MODULE_SEBEK_CHECKER5_H
#define MODULE_SEBEK_CHECKER5_H

#include "Checker.h"

class Checker5 : public Checker{
public:
    bool check(std::int32_t number) const noexcept final;
};


#endif //MODULE_SEBEK_CHECKER5_H
