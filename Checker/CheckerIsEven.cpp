#include "CheckerIsEven.h"

bool CheckerIsEven::check(std::int32_t number) const noexcept {
    return number%2==0;
}
