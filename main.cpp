#include <iostream>
#include <vector>
#include <random>
#include <memory>
#include <cstdint>
#include <algorithm>
#include <string_view>
#include "Checker/Checker.h"
#include "Checker/Checker5.h"
#include "Checker/CheckerIsEven.h"

using CheckerRange = std::vector<std::shared_ptr<Checker>>;

std::vector<std::int32_t> generateRange(){
    std::random_device rd;
    std::uniform_int_distribution<std::int32_t> dist(-10, 10);

    std::vector<std::int32_t> result;
    size_t result_size = 20;
    result.resize(result_size);

    std::generate(result.begin(), result.end(), [&rd, &dist](){
        return dist(rd);
    });
    return result;
}

void checkRange(std::vector<std::int32_t>& range, const CheckerRange& checkerRange){
    auto last_it = std::remove_if(range.begin(), range.end(), [&checkerRange](auto number){
        return !std::all_of(checkerRange.begin(), checkerRange.end(), [number](const auto &checker){
            return checker->check(number);
        });
    });
    range.erase(last_it, range.end());
}

void printRange(const std::vector<std::int32_t>& range, std::string_view msg = ""){
    std::cout<<msg;
    for (const auto& it: range){
        std::cout<<" [ "<<it<<"]";
    }
    std::cout<<std::endl;
}

int main() {
    auto range = generateRange();

    printRange(range, "generated range: ");

    const CheckerRange checkers = {std::make_shared<Checker5>(), std::make_shared<CheckerIsEven>()};
    checkRange(range, checkers);

    printRange(range, "filtered range: ");
    return 0;
}
