cmake_minimum_required(VERSION 3.19)
project(module_sebek)

set(CMAKE_CXX_STANDARD 17)

add_executable(module_sebek main.cpp Checker/Checker.h Checker/Checker5.cpp Checker/Checker5.h Checker/CheckerIsEven.cpp Checker/CheckerIsEven.h)